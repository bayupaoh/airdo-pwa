
export default function NavBar() {
  return (
    <nav className="bg-white border-gray-200 dark:bg-gray-900">
      <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <a href="#" className="flex items-center">
          <img src="logo-thanq.png" className="h-16 mr-3" alt="Thanq Logo" />
          <div className="flex flex-col">
            <span className="text-2xl font-semibold text-sky-500 whitespace-nowrap dark:text-white">ThanQ</span>
            <span className="text-s font-light whitespace-nowrap dark:text-white">Pesan Tangki Online - Kota Kupang</span>
          </div>
        </a>
        <div className="hidden w-full md:block md:w-auto" id="navbar-default">
        </div>
      </div>
    </nav>
  )
}  