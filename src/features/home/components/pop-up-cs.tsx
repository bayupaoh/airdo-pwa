import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

export function PopUpCS() {
    return (
        <div className="fixed bottom-4 right-4 animate-bounce flex items-center">
            <div className="group-hover:opacity-0 transition-opacity opacity-100 bg-gray-800 px-1 text-sm text-gray-100 rounded-md p-1">Tertarik jadi Mitra?</div>
            <Link href={"https://forms.gle/shxDrbQiXHzMogoY6"} className="rounded-full w-12 h-12 bg-sky-500">
            <FontAwesomeIcon icon={faPhone} className="text-white" />
            </Link>
        </div>
    )
}