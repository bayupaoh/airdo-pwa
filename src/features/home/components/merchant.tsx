import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import { faCar, faClock, faGlassWater, faPhone, faUser, faWater } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

interface MerchantItem {
    name: string
    deliveryArea: string
    waterFillingLocation: string
    plat: string
    volume: string
    phone: string
    isUseWhatsapp: boolean
    photo: string
}

export function Merchant(item: MerchantItem) {
    return (
        <div className="w-full sm:w-1/2 md:w-1/2 xl:w-1/4 p-4">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 m-2">
                <a href="#">
                    <img className="h-36 object-none w-full rounded-t-lg" src="logo-thanq.png" alt="" />
                </a>
                <div className="p-5">
                    <a href="#">
                        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{item.name}</h5>
                    </a>
                    <div className="mb text-m font-bold tracking-tight text-gray-900 dark:text-white">
                        Daerah Pengantaran
                    </div>
                    <div className="mb-2 text-l font-normal tracking-tight text-gray-900 dark:text-white">
                        {item.deliveryArea}
                    </div>
                    <div className="mb text-m font-bold tracking-tight text-gray-900 dark:text-white">
                        Tempat Pengisian Air
                    </div>
                    <div className="mb-2 text-l font-normal tracking-tight text-gray-900 dark:text-white">
                        {item.waterFillingLocation}
                    </div>
                    <div className="text-xs text-gray-700">
                        <span className="flex items-center mb-1">
                            <FontAwesomeIcon icon={faCar} className="far mr-2 text-gray-900" /> {item.plat}
                        </span>
                        <span className="flex items-center mb-1">
                            <FontAwesomeIcon icon={faGlassWater} className="far mr-2 text-gray-900" /> {item.volume}
                        </span>
                    </div>
                    <Link href={"tel:" + item.phone} className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        <FontAwesomeIcon icon={faPhone} className="mr-2 text-white" /> Telepon
                    </Link>
                    {item.isUseWhatsapp ?
                        <Link href={"https://wa.me/" + item.phone} className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-green-400 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            <FontAwesomeIcon icon={faWhatsapp} className="mr-2 text-white" /> Whatsapp
                        </Link>
                        : ""}
                </div>
            </div>
        </div>
    )
}