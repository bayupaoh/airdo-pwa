
export function Filter() {
    return (
        <div className="sticky top-0 p-4 bg-white w-full">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Pilih Daerah Pengantaran</label>
            <select className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                <option selected value="">Semua</option>
                <option value="Alak">Kec. Alak</option>
                <option value="Kota Raja">Kec. Kota Raja</option>
                <option value="Kota Lama">Kec. Kota Lama</option>
                <option value="Maulafa">Kec. Maulafa</option>
                <option value="Oebobo">Kec. Oebobo</option>
                <option value="Alak">Kec. Alak</option>
            </select>
        </div>
    )
}