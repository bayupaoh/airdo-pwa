import NavBar from './components/navbar'
import { Merchant } from './components/merchant'
import { Filter } from './components/filter'
import { PopUpCS } from './components/pop-up-cs'
import { useEffect, useState } from 'react'

export default function Home() {
  const [merchants, setMerchants] = useState<any[]>([])

  async function getData() {
    const response = await fetch("/api/merchants")
    const jsonData = await response.json()
    setMerchants(jsonData)
  }

  useEffect(() => {
    getData()
  }, [])
  /*
     "timestamp": "27/03/2023 9:14:27",
      "name": "Maxi Finit",
      "phone": "081338953762",
      "is_whatsapp": "Tidak",
      "plat": "DH 8671 AF",
      "tank_expired": "01 - 28",
      "volume": "5000 liter",
      "photo": "https://drive.google.com/open?id=12FxlZokFO4xhY0IiHD7okHZy7E_FHmgF",
      "water_filling_location": "Depo Air Liliba",
      "delivery_area": "Kec. Alak, Kec. Kelapa Lima, Kec. Kota Raja, Kec. Kota Lama, Kec. Maulafa, Kec. Oebobo",
      "ownership_status": "Milik Orang Lain (Hanya Driver)"
  */
  return (
    <div>
      <NavBar />
      <div className="container mx-auto">
        <Filter />
        <div className="flex flex-wrap -mx-4">
          {merchants.map(merchant =>
            <Merchant
              name={merchant.name}
              deliveryArea={merchant.delivery_area}
              waterFillingLocation={merchant.water_filling_location}
              plat={merchant.plat}
              volume={merchant.volume}
              phone={merchant.phone}
              isUseWhatsapp={merchant.is_whatsapp == 'Ya'}
              photo={''}
            />)}
        </div>
      </div>
      <PopUpCS />
    </div>
  )
}
